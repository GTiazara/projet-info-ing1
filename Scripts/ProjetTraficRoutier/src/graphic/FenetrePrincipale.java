/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphic;

/**
 * @author tiazara
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import javax.swing.BorderFactory;
import javax.swing.JFrame;

public class FenetrePrincipale extends JFrame implements KeyListener{
    // définition des attributs de taille de la fenêtre principale
    private final int WIDTH = 1200;    // Largeur 
    private final int HEIGHT = 700;    // hauteur
    private Dimension SCREEN_SIZE = new Dimension(WIDTH, HEIGHT);    // dimension
    private Color bgColor = new  Color(116, 121, 125);     // couleur de fond
    private Color bdColor = new  Color(116, 121, 125);    // couleur du bord
    private PanneauSimulation simul;
    public PanneauControl pC;

    public FenetrePrincipale() throws IOException{
        this.init();   
    }
    
    // initialisation de la fenêtre Principale
    public void init () throws IOException{
        this.setTitle("Simulation Trafic Routier");
        this.setSize (SCREEN_SIZE);
        this.setFocusable(true);     // pour forcer la détection d'évenement clavier
        this.setResizable(false);    // interdit le redimensionnement de la fenêtre principale
        this.setLocationRelativeTo ( null );   // centre l'affichage de la fenêtre principale
        this.getRootPane().setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5,bdColor));   //mise en place de bordure de la fenêtre principale 
        this.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);    // fermeture de la fenêtre principale
        // Obltenir le conteneur de la fenêtre principal et le personnalser
        Container ConteneurPrincipal = this.getContentPane();  
        ConteneurPrincipal.setBackground(bgColor);
        // Creation d'un grille Nord, Sud, Est, Ouest
        // Chaque élément sera séparer de 2 pix la largeur et hauteur 
        this.setLayout(new BorderLayout(2, 2));
        // creation du panneau de controle et ajout à la fenêtre principale
        this.simul = new PanneauSimulation();
        this.pC = this.simul.ctl;
        this.add(pC, BorderLayout.WEST);
        this.add(this.simul, BorderLayout.EAST); 
        this.addKeyListener(this);
    }

    @Override
    public void keyTyped(KeyEvent e) {}
    @Override
    public void keyPressed(KeyEvent e) {
        System.out.println("Pressed");
        simul.getKey(e);
    }
    @Override
    public void keyReleased(KeyEvent e) {}
}

    
    
    





