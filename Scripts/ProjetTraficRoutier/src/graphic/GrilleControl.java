/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphic;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 *
 * @author tiazara
 */

public class GrilleControl extends JPanel{
    // Finalement, on ne mets que le logo de l'ensg parce que les bouttons et les champs de saisi ...etc prendra trop de temps à mettre en place 
    private BufferedImage img;
    public GrilleControl(String filename) throws IOException{
        this.setPreferredSize(new Dimension(400, 400));

        File f = new File(filename);
        try {
        img = ImageIO.read(f);
        }
        catch (IOException e) {System.out.println("Problème de fichier");}
    }   
    @Override
    public void paintComponent(Graphics g) {
        g.drawImage(this.img, 12, 12, 370, 200, this);

    }  
}
