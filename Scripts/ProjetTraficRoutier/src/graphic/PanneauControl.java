/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphic;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.io.IOException;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

/**
 *
 * @author tiazara
 */

public class PanneauControl extends JPanel{
    // definition des attributs de taille du panneau de contrôle
    private final int WIDTH = 400;    //Largeur 
    private final int HEIGHT = 700;    //hauteur
    private Dimension CPAN_SIZE = new Dimension(WIDTH, HEIGHT);    //dimension
    private Color bgColor = new  Color(116, 121, 125);     //couleur de fond
    private VisualisationGrpahe graph;
    public GrilleControl grilleControl;
    public PanneauControl() throws IOException{      
        this.init();
        
    }
    // initialisation du panneau de control
    public void init() throws IOException{   
        this.setPreferredSize(CPAN_SIZE);
        this.setBorder(new LineBorder(Color.BLACK, 1));
        // Creation d'un grille Nord, Sud, Est, Ouest
        this.setLayout(new BorderLayout(5, 5));
        //this.setLayout(new FlowLayout());
        this.setBackground(bgColor);
        grilleControl = new GrilleControl("Image/ENSG.png");
        graph = new VisualisationGrpahe();
        graph.setBackground(bgColor);
        // Ajout de la grille de controle dans le panneau de controle
        this.add(grilleControl, BorderLayout.NORTH);
        this.add(graph, BorderLayout.SOUTH);
    }
    
    public VisualisationGrpahe getGraph(){return this.graph;}


    }
