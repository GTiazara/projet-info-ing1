/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphic;

import simulation.Simulation;
import road.Route;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import road.Voiture;

/**
 *
 * @author tiazara
 */

public class PanneauSimulation extends JPanel implements Runnable{  
    // definition des attributs de taille de la fenêtre de simulation
    private final int WIDTH = 768;    //Largeur 
    private final int HEIGHT = 700;    //hauteur
    private Dimension SIMUL_PAN_SIZE = new Dimension(WIDTH, HEIGHT);    //dimension
    private Color bgColor = new  Color(10, 10, 70);     //couleur de fond
    public PanneauControl ctl;    // permet de prendre et de mettre à jour les éléments du panneau de control
    private Simulation simulTraffic;
    private Thread simulationTime; 
    private BufferedImage imgCompas;

    public PanneauSimulation () throws IOException{
        this.init();
        this.simulationTime = new Thread(this);
        simulationTime.start();   
        this.simulTraffic.InitVoitureOnRoadTraffic();
        File f = new File("Image/compas3.png");
        try {
        this.imgCompas = ImageIO.read(f);
        }
        catch (IOException e) {System.out.println("Problème de fichier");}
    }
    
    // initialisation du panneau de simulation
    public void init() throws IOException{  
        this.setPreferredSize(SIMUL_PAN_SIZE);
        this.setBorder(new LineBorder(Color.BLACK, 1));
        this.setBackground(bgColor);
        this.simulTraffic = new Simulation();    
        ctl = new PanneauControl();
    }
    // Affiche les objet dessiner
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        for(Route R: this.simulTraffic.getListRoute() ){
        	R.draw(g);
        }
        for(Route R: this.simulTraffic.getListRoute() ){
                R.drawCarOnRoad(g);
        }
        
        g.drawImage(this.imgCompas, 680, 12, 80, 80, this);
        
    }
    

    @Override
    public void run() {
        int timeFeuHms = this.simulTraffic.getTimeFeuHsec() * 1000;
        int timeFeuVms = this.simulTraffic.getTimeFeuVsec() * 1000;
        int timeAddVoiturems = this.simulTraffic.getTimeAddVoiture() * 1000;
        int maxCountTime = 60;    // temps en seconde  d'arret de comptage de voiture arreter par le feu rouge 
        int GreenTime = timeFeuHms;
        int compteurFeu = 0;
        int compteurVoiture = 0;
        int compteurmMaxCountTime  = 0;
        int sleepTime = 40;     //en milliseconde
        int NbVehiculeStopedOnRedSignal = 0;
        int NbVehiculeStopedOnRedSignalR0 = 0;
        int NbVehiculeStopedOnRedSignalR1 = 0;
        int NbVehiculeStopedOnRedSignalR2 = 0;
        int NbVehiculeStopedOnRedSignalR3 = 0;
        
        // boucle de simulation
        while (true){
            try {
                Thread.sleep(sleepTime); // faire des horloges à partir du même thread en créant des variable entier.
                compteurFeu += 1; 
                compteurVoiture += 1; 
                compteurmMaxCountTime += 1;
                // changement des couleurs des feux et dénombrement des voiture arréter au feu rouge
                if (compteurFeu * sleepTime >= GreenTime){
                    for(Route R: this.simulTraffic.getListRoute()){
                        R.getFeu().changeEtat(R.getFeu().getEtat());
                        
                        for (Voiture v: R.getListVoiture()){
                            if (v.getDeltaPosition() == 0){
                                NbVehiculeStopedOnRedSignal += 1;
                                ctl.getGraph().updateGraphe(NbVehiculeStopedOnRedSignal, "Tot", "Road id", "Nombre de voiture arretées au feu rouge", "Nombre de voiture arreté");

                                if (R.getId() == 0){
                                    NbVehiculeStopedOnRedSignalR0 += 1;
                                    ctl.getGraph().updateGraphe(NbVehiculeStopedOnRedSignalR0, "R" + R.getId(), "Road id", "Nombre de voiture arretées au feu rouge", "Nombre de voiture arreté");
                                }
                                if (R.getId() == 1){
                                    NbVehiculeStopedOnRedSignalR1 += 1;
                                    ctl.getGraph().updateGraphe(NbVehiculeStopedOnRedSignalR1, "R" + R.getId(), "Road id", "Nombre de voiture arretées au feu rouge", "Nombre de voiture arreté");
                                }
                                if (R.getId() == 2){
                                    NbVehiculeStopedOnRedSignalR2 += 1;
                                    ctl.getGraph().updateGraphe(NbVehiculeStopedOnRedSignalR2, "R" + R.getId(), "Road id", "Nombre de voiture arretées au feu rouge", "Nombre de voiture arreté");
                                }
                                if (R.getId() == 3){
                                    NbVehiculeStopedOnRedSignalR3 += 1;
                                    ctl.getGraph().updateGraphe(NbVehiculeStopedOnRedSignalR3, "R" + R.getId(), "Road id", "Nombre de voiture arretées au feu rouge", "Nombre de voiture arreté");
                                }             
                            }
                            if (compteurmMaxCountTime * sleepTime <= maxCountTime * 1000){
                                ctl.getGraph().updateGraphe(NbVehiculeStopedOnRedSignal, "TotOnMaxTime", "Road id", "Nombre de voiture arretées au feu rouge", "Nombre de voiture arreté");
                            }
                        }
                        
                    }
                    compteurFeu = 0;
                    if (GreenTime == timeFeuHms){
                        GreenTime = timeFeuVms;
                    }
                    else if (GreenTime == timeFeuVms){
                        GreenTime = timeFeuHms;
                    }
                    
                }
                // changement des couleur des feu et dénombrement des voiture arréter au feu rouge
                if ((compteurVoiture * sleepTime >= timeAddVoiturems)){
                    this.simulTraffic.addVoitureOnRandomRoad();
                    compteurVoiture  =0;
                }
                
                this.simulTraffic.roadTrafic();
                repaint();
            } 
            catch (InterruptedException ex) {
                Logger.getLogger(PanneauSimulation.class.getName()).log(Level.SEVERE, null, ex);
            }
                  
        }
    }
    
    public void getKey(KeyEvent e){
        this.simulTraffic.keyPressed(e);
    }
}
