/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulation;
import road.Route;
import road.Voiture;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.locationtech.jts.geom.Coordinate;
import road.FeuTricolor;

public class Simulation { 
    
    private List<Voiture> lsVoiture;
    private int key;
    private List<Route> lsRoute;
    private String[][] lsFileVoiture;
    private int timeFeuHsec;
    private int timeFeuVsec;
    private int timeAddVoiture;
    private int nbVoitureOnroad;
    private int nbVoitureOnroadMax;
    
    public Simulation(){ 
        this.timeFeuHsec = 5;   //seconde
        this.timeFeuVsec = 5;   //seconde
        this.timeAddVoiture = 2;    //seconde 
        this.nbVoitureOnroad = 4;   //nombre de voiture initiale sur la route 
        this.nbVoitureOnroadMax = 25;
        this.init();
    }
    // Initialisation des objets routier
    public void init(){
        this.initListeVoiture(); 
        this.initialisationRoute();
    }
    
    // Initilaisation des routes
    public void initialisationRoute() {
    	Coordinate carfour = new Coordinate(400,350);
    	FeuTricolor F0 = new FeuTricolor(1, 10,"vert");
    	FeuTricolor F1 = new FeuTricolor(2,10,"rouge");
    	FeuTricolor F2 = new FeuTricolor(3,10,"vert");
    	FeuTricolor F3 = new FeuTricolor(4,10,"rouge");
        // Pour facilité la gestion des routes et des feu: Horizontale pair, route vertical: id impair
    	Route R0 = new Route(0, 6,new Coordinate(0,350), carfour, F0); 
    	Route R2 = new Route(2, 6,new Coordinate(800,350), carfour, F2);
    	Route R1 = new Route(1, 6,new Coordinate(400,0), carfour, F1);
    	Route R3 = new Route(3, 6 , new Coordinate(400,700),carfour,F3);
    	lsRoute = new ArrayList();
    	lsRoute.add(R0);
    	lsRoute.add(R2);
    	lsRoute.add(R1);
    	lsRoute.add(R3);
    }
    
    public List<Route> getListRoute() {
    	return this.lsRoute;
    }
    // Initilaisation des voiture
    public void initListeVoiture(){
        Random rd = new Random();
        lsVoiture = new ArrayList();
        String[] v0img = new String[]{"Image/Car5_orange/car_orange_haut.png", "Image/Car5_orange/car_orange_droite.png", "Image/Car5_orange/car_orange_bas.png", "Image/Car5_orange/car_orange_gauche.png"};
        String[] v1img = new String[]{"Image/Car4_bus_bleu/bus2_bleu_haut.png", "Image/Car4_bus_bleu/bus2_bleu_droite.png", "Image/Car4_bus_bleu/bus2_bleu_bas.png", "Image/Car4_bus_bleu/bus2_bleu_gauche.png"};
        String[] v3img = new String[]{"Image/Car3_bus_bleu/bus_bleu_haut.png", "Image/Car3_bus_bleu/bus_bleu_droite.png", "Image/Car3_bus_bleu/bus_bleu_bas.png", "Image/Car3_bus_bleu/bus_bleu_gauche.png"};
        String[] v2img = new String[]{"Image/Car1_rouge/car_haut.png", "Image/Car1_rouge/car_droite.png", "Image/Car1_rouge/car_bas.png", "Image/Car1_rouge/car_gauche.png"};
        String[] v4img = new String[]{"Image/Car2_jaune/car_jaune_haut.png", "Image/Car2_jaune/car_jaune_droite.png", "Image/Car2_jaune/car_jaune_bas.png", "Image/Car2_jaune/car_jaune_gauche.png"};
        this.lsFileVoiture = new String[][]{v0img, v1img, v2img, v3img, v4img};    //liste des apparences de 5 voitures
        String[] lsDirection = new String[]{"N", "S", "E", "O"};
        Voiture v0 = new Voiture(v0img, 0, 50, new Coordinate(100, 100), new Dimension(50, 50), "E");
        Voiture v1 = new Voiture(v1img, 1, 50, new Coordinate(200, 200), new Dimension(50, 50), "O");
        Voiture v2 = new Voiture(v2img, 2, 50, new Coordinate(300, 100), new Dimension(50, 50), "S");
        Voiture v3 = new Voiture(v3img, 3, 50, new Coordinate(300, 300), new Dimension(50, 50), "N");
        // 4 voitures bien choisi pour les tester sur la route 
        this.lsVoiture.add(v0);
        this.lsVoiture.add(v1);
        this.lsVoiture.add(v2);
        this.lsVoiture.add(v3);
        for (int i = 4; (i < this.nbVoitureOnroadMax); i++){
            
            Voiture v = new Voiture(this.lsFileVoiture[rd.nextInt(4)], i, 50, new Coordinate(100, 100), new Dimension(50, 50), lsDirection[rd.nextInt(3)]);
            this.lsVoiture.add(v);
        }
        
    }       
    public List<Voiture> getListeVoiture(){ return (this.lsVoiture);} 
    
    public void InitVoitureOnRoadTraffic(){

        this.lsRoute.get(0).addVoiture(lsVoiture.get(0));
        this.lsRoute.get(0).initPositionVoiture(this.lsRoute.get(0).getListVoiture().get(0));
        
        this.lsRoute.get(1).addVoiture(lsVoiture.get(1));
        this.lsRoute.get(1).initPositionVoiture(this.lsRoute.get(1).getListVoiture().get(0));
        
        this.lsRoute.get(2).addVoiture(lsVoiture.get(2));
        this.lsRoute.get(2).initPositionVoiture(this.lsRoute.get(2).getListVoiture().get(0));
        
        this.lsRoute.get(3).addVoiture(lsVoiture.get(3));
        this.lsRoute.get(3).initPositionVoiture(this.lsRoute.get(3).getListVoiture().get(0));
        
    }
    
    public void addVoitureOnRandomRoad(){
        Random rd = new Random();
        int randRoute = rd.nextInt(4);
        Route R = this.lsRoute.get(randRoute);
        Voiture v;
        if (this.nbVoitureOnroad < this.nbVoitureOnroadMax - 1){
            this.nbVoitureOnroad += 1;
            v = this.lsVoiture.get(this.nbVoitureOnroad);
            if (R.getId() == 0){
                v.setDirection("E");
            }
            else if (R.getId() == 2){
                v.setDirection("O");
            }
            else if (R.getId() == 1){
                v.setDirection("S");
            }
            else if (R.getId() == 3){
                v.setDirection("N");
            }
            if (!R.getListVoiture().contains(v)){
                this.lsRoute.get(randRoute).addVoiture(v);
            }
            int lastVoiture = R.getListVoiture().size() - 1;
            R.initPositionVoiture(R.getListVoiture().get(lastVoiture));
        }
        // Si tous les voitures de la liste initiale ont été mis en cicrculation, on "recycle" les voitures qui sont déja en circulation pour que l'application continue de fonctionner 
        else{
            v = R.getListVoiture().get(0);
            R.initPositionVoiture(v);
            R.getListVoiture().remove(0);
            R.getListVoiture().add(v);
        }   
    }    
    
    public void roadTrafic(){
        for (int i = 0; (i < 4); i++){
            for (Voiture v: this.lsRoute.get(i).getListVoiture()){
                this.lsRoute.get(i).driveOnRoad(v);
            }
        }
    }
    
    public int getTimeFeuHsec(){return(this.timeFeuHsec);}
    public int getTimeFeuVsec(){return(this.timeFeuVsec);}
    public int getTimeAddVoiture(){return(this.timeAddVoiture);}
    
    // finalement on ne s'en servira pas !
    public void keyPressed(java.awt.event.KeyEvent e) {
        this.key = e.getKeyCode();
        System.out.println(key);
    }
}

  
